(
    rust_numeric_version () {
        printf "\"%s\"" $2
    }
    rust_numeric_version $(rustc --version)
)